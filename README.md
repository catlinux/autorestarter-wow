# AutoRestarter para emuladores de Wow

***AutoRestarter*** nos da la ventaja de poder conectar a la consola del servidor desde cualquier lugar, incluso del exterior si tines acceso por ***SSH*** y si por algún motivo muere el proceso, se encargará de reiniciarlo, tanto el emulador del reino como el servidor de loging.

Este [post](https://catlinux.es/autorestarter-para-emuladores-de-wow/) explica más detalladamente su utilización.

***NO SOY EL AUTOR*** de este script, lo encontré en la red, pero no sé quién es creador. En el final del post pondré un enlace a la página donde lo encontré.
* Requisitos
* Descarga
* Ajustes
* Uso

### Requisitos
Para poder utilizar el script necesitamos instalar los paquetes ***screen*** y ***gdb***.

Debian y derivados:
> sudo apt install screen gdb

Archlinux:
> sudo pacman -S screen gdb

### Descarga
Puedes copiarlo directamente del código o descargarlos de aquí: [wowadmin.sh](https://gitlab.com/catlinux/autorestarter-wow/-/archive/master/autorestarter-wow-master.zip)

### Ajustes
`APATH=/ruta/de/tu/authserver   #Ruta dek directorio donde se encuentra el authserver`

`WPATH=/ruta/de/tu/worldserver  #Ruta dek directorio donde se encuentra el worldserver`

`ASRV_BIN=authserver            #Escoger según el emulador. TrinityCore: authserver  MaNGOS: realmd`

`WSRV_BIN_ORG=worldserver       #Escoger según el emulador. TrinityCore: worldserver MaNGOS: mangosd`

`WSRV_BIN=worldserver           #Escoger según el emulador. TrinityCore: worldserver MaNGOS: mangosd`

`WSRV_SCR=worldserver           #Escoger según el emulador. TrinityCore: worldserver MaNGOS: mangosd`

### Uso
Ejecuta el comando seguido de la opción correspondiente.

Las opciones son:

* start : Inicia los servicios authserver y worldserver.
* stop : Detiene los servicios authserver y worldserver.
* restart : Reinicia los servicios authserver y worldserver.
* wstart : Inicia tan solo el servicio worldserver.
* wdstart : Inicia el servicio worldserver en modo DEBUG.
* wrestart : Reinicia el servicio worldserver.
* wstop : Detiene el servicio worldserver.
* wmonitor : Muestra en consola el proceso worldserver.
* astart : Inicia el servicio authserver.
* arestart : Reinicia el servicio authserver.
* astop : Detiene los servicios authserver y worldserver.
* amonitor: Muestra en consola el proceso authserver.

#### Fuentes:

https://www.getmangos.eu/

https://catlinux.es/autorestarter-para-emuladores-de-wow/
