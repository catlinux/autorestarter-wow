#!/bin/bash

THIS_FULLPATH=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd -P)/`basename "${BASH_SOURCE[0]}"`
THIS_FOLDERPATH=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd -P)

APATH=/ruta/de/tu/authserver    #Ruta del directorio donde se encuentra el authserver
WPATH=/ruta/de/tu/worldserver   #Ruta del directorio donde se encuentra el worldserver
ASRV_BIN=authserver             #Escoger según el emulador. TrinityCore: authserver  MaNGOS: realmd
WSRV_BIN_ORG=worldserver        #Escoger según el emulador. TrinityCore: worldserver MaNGOS: mangosd
WSRV_BIN=worldserver            #Escoger según el emulador. TrinityCore: worldserver MaNGOS: mangosd
WSRV_SCR=worldserver            #Escoger según el emulador. TrinityCore: worldserver MaNGOS: mangosd

echo "run" > gdbcommands
echo "shell echo -e \"\nCRASHLOG BEGIN\n\"" >> gdbcommands
echo "info program" >> gdbcommands
echo "shell echo -e \"\nBACKTRACE\n\"" >> gdbcommands
echo "bt" >> gdbcommands
echo "shell echo -e \"\nBACKTRACE FULL\n\"" >> gdbcommands
echo "bt full" >> gdbcommands
echo "shell echo -e \"\nTHREADS\n\"" >> gdbcommands
echo "info threads" >> gdbcommands
echo "shell echo -e \"\nTHREADS BACKTRACE\n\"" >> gdbcommands
echo "thread apply all bt full" >> gdbcommands

DEBUG=false

#WORLD FUNCTIONS
startWorld()
{
    if [ "$(screen -ls | grep $WSRV_SCR)" ]
    then
        echo $WSRV_BIN ya está en ejecución
    else
        cd $WPATH
        screen -AmdS $WSRV_SCR $THIS_FULLPATH $WSRV_BIN $DEBUG
        echo $WSRV_BIN en marcha
    fi
}

restartWorld()
{
    screen -S $WSRV_SCR -X stuff "saveall$(printf \\r)"
    echo salvados todos los personajes y se empieza el reinicio del servidor
    screen -S $WSRV_SCR -X stuff "server restart 5$(printf \\r)"
}

stopWorld()
{
    screen -S $WSRV_SCR -X stuff "saveall
    "
    echo guardado total enviado, esperando 5 segundos para matar el proceso $WSRV_BIN
    sleep 5
    screen -S $WSRV_SCR -X kill &>/dev/null
    echo $WSRV_BIN está muerto
}

monitorWorld()
{
    echo presione ctrl + a + d para desconectarse del servidor sin apagarlo
    sleep 5
    screen -r $WSRV_SCR
}
#AUTH FUNCTIONS
startAuth()
{
    if [ "$(screen -ls | grep $ASRV_BIN)" ]
    then
        echo $ASRV_BIN ya está funcionando
    else
        cd $APATH
        screen -AmdS $ASRV_BIN $THIS_FULLPATH $ASRV_BIN
        echo $ASRV_BIN está en marcha
    fi
}

stopAuth()
{
    screen -S $ASRV_BIN -X kill &>/dev/null
    echo $ASRV_BIN está muerto
}

restartAuth()
{
    stopAuth
    startAuth
    echo $ASRV_BIN reiniciado
}

monitorAuth()
{
    echo presione ctrl + a + d para desconectarse del servidor sin apagarlo
    sleep 5
    screen -r $ASRV_BIN
}

#FUNCTION SELECTION
case "$1" in
    $WSRV_BIN )
    if [ "$2" == "true" ]
    then
        while x=1;
        do
            gdb $WPATH/$WSRV_BIN --batch -x gdbcommands | tee current
            NOW=$(date +"%s-%d-%m-%Y")
            mkdir -p $THIS_FOLDERPATH/crashes
            mv current $THIS_FOLDERPATH/crashes/$NOW.log &>/dev/null
            killall -9 $WSRV_BIN
            echo $NOW $WSRV_BIN parado, reiniciando! | tee -a $THIS_FULLPATH.log
            echo crashlog disponible en: $THIS_FOLDERPATH/crashes/$NOW.log
            sleep 1
        done
    else
        while x=1;
        do
            ./$WSRV_BIN
            NOW=$(date +"%s-%d-%m-%Y")
            echo $NOW $WSRV_BIN parado, reiniciando! | tee -a $THIS_FULLPATH.log
            sleep 1
        done
    fi
    ;;
    $ASRV_BIN )
        while x=1;
        do
            ./$ASRV_BIN
            NOW=$(date +"%s-%d-%m-%Y")
            echo $NOW $ASRV_BIN parado, reiniciando! | tee -a $THIS_FULLPATH.log
            sleep 1
        done
    ;;
    "wstart" )
    startWorld
    ;;
    "wdstart" )
    DEBUG=true
    startWorld
    ;;
    "wrestart" )
    restartWorld
    ;;
    "wstop" )
    stopWorld
    ;;
    "wmonitor" )
    monitorWorld
    ;;

    "astart" )
    startAuth
    ;;
    "arestart" )
    restartAuth
    ;;
    "astop" )
    stopAuth
    ;;
    "amonitor" )
    monitorAuth
    ;;

    "start" )
    startWorld
    startAuth
    ;;
    "stop" )
    stopWorld
    stopAuth
    ;;
    "restart" )
    restartWorld
    restartAuth
    ;;
    * )
    echo Especifica una opción - wowadmin.sh [opcion]
    echo "opciones: start | stop | restart | wstart | wdstart | wrestart | wstop | wmonitor | astart | arestart | astop | amonitor"
    exit 1
    ;;
esac
